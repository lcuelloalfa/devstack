Open edX Devstack 
=================
Este proyecto es un fork de [edX devstack](https://github.com/edx/devstack) para Campus Digital, se han hecho algunas modificaciones al archivo repo.sh (se cambio el repositorio [edX analytics pipeline](https://bitbucket.org/lcuelloalfa/edx-analytics-pipeline) y se solucionó un error al ejecutar `make dev.clone`

En este repositorio se incluyen todos los servicios de Open edX.

Para su información
-------------------
Todos los comandos `make` descritos más abajo se deben ejecutar en la máquina local, no desde una VM.

Prerrequisitos
--------------
Este proyecto requiere **Docker 17.06+ CE**

También se requiere que las siguientes herramientas estén instaladas:

- make
- python pip (opcional para MacOS)
- docker-compose

Introducción
------------

A continuación se encuentran las instrucciones para la instalacion de **edX Devstack.**

Esta guía de instalación fue separada en dos secciones ya que se incluyen dos tipos de servicio: un devstack con todos los servicios de edX y otro devstack con los servicios de edX Analytics Pipeline, hay una guía de instalación para ambos servicios, descrita más abajo.

Si bien cada servicio opera por sí sólo, para poder ejecutar algunas tareas, el Pipeline require acceso al LMS y para esto se debe configurar la REST API de edX a través de un cliente Oauth2. 

Para probar las tareas, se recomienda realizar ambas instalaciones.

Instalación de edX Devstack
---------------------------

Esta instalación fue realizada y probada utilizando la distribución Testing GNU/Linux Debian Buster

### Imágenes y repositorios edX Devstack ###

Al instalar desde este repositorio se incluyen las siguientes imágenes de **Docker**: 

Extraído desde `docker-compose.yml`:

- [chrome](https://hub.docker.com/r/edxops/chrome)
- [elasticsearch](https://hub.docker.com/r/edxops/elasticsearch)
- [firefox](https://hub.docker.com/r/edxops/firefox)
- [memcached](https://hub.docker.com/_/memcached)
- [mongodb](https://hub.docker.com/_/mongo)
- [mysql](https://hub.docker.com/_/mysql)
- [studio](https://hub.docker.com/r/edxops/edxapp)
- [lms](https://hub.docker.com/r/edxops/edxapp)
- [forum](https://hub.docker.com/r/edxops/forum)
- [devpi](https://hub.docker.com/r/edxops/devpi)

Tambien utiliza los siguientes repositorios:

- [credentials](https://github.com/edx/credentials.git)
- [course-discovery](https://github.com/edx/course-discovery.git)
- [ecommerce](https://github.com/edx/ecommerce.git)
- [edx-e2e-tests](https://github.com/edx/edx-e2e-tests.git)
- [edx-notes-api](https://github.com/edx/edx-notes-api.git)
- [edx-platform](https://github.com/edx/edx-platform.git)
- [xqueue](https://github.com/edx/xqueue.git)
- [gradebook](https://github.com/edx/gradebook.git)
- [lms](https://github.com/edx/edx-platform.git)

### Instalación edX Devstack ###

Para la instalación de Devstack, seguir las siguientes instrucciones:

1. Clonar este repositorio.

	```	
	git clone https://bitbucket.org/lcuelloalfa/devstack
	```

2. Navegar al directorio `devstack`

	```
	cd devstack
	```

3. Si no está utilizando la rama master, defina una variable de entorno para la versión de **Open edX** que está utilizando, como `hawthorn.master`, la más estable a la fecha.

	```
	export OPENEDX_RELEASE=hawthorn.master
	```

4. Clonar los repositorios de **Open edX**. El archivo docker-compose monta un **volumen** por cada servicio. Por defecto, los repositorios son clonados un directorio sobre el directorio `devstack`. Por ejemplo, si se ejecuta en `workspace/devstack`, los repositorios serán clonados en la carpeta `workspace`

	```
	make dev.clone
	```

	Para cambiar el directorio por defecto en donde serán clonados los repositorios, asignar la variable de entorno `DEVSTACK_WORKSPACE`

5. (Sólo macOS) Compartir los directorios de los servicios clonados en Docker, usando **Docker -> Preferencias -> Compartir Archivos** en el menú Docker. 

6. Ejecutar el comando de provision para descargar las imágenes **Docker**, crear los contenedores y configurar los variados servicios con superusuarios. 

	```
	make dev.provision
	```

	Si se desea instalar edX Devstack junto a edX Analytics Pipeline ejecutar `make dev.provision && make dev.provision.analytics_pipeline`

	**NOTA:** Cuando se ejecuta el comando de provision, las bases de datos para ecommerce y edxapp serán eliminadas y recreadas.

7. Iniciar los contenedores:

	```
	make dev.up
	```
	
	Para iniciar edX Devstack junto a edX Analytics Pipeline ejecutar `make dev.up && make dev.up.analytics_pipeline`

8. Para detener los contenedores:

	```
	make stop
	``` 

Instalación de Analytics Devstack
---------------------------------

### Imágenes y repositorios Analytics Devstack ###

Máquinas **Docker** que utiliza **Analytics Devstack**

Extraído desde `docker-compose-analytics-pipeline.yml`:

- [namenode](https://hub.docker.com/r/edxops/analytics_pipeline_hadoop_namenode)
- [datanode](https://hub.docker.com/r/edxops/analytics_pipeline_hadoop_datanode)
- [resourcemanager](https://hub.docker.com/r/edxops/analytics_pipeline_hadoop_resourcemanager)
- [nodemanager](https://hub.docker.com/r/edxops/analytics_pipeline_hadoop_nodemanager)
- [sparkmaster](https://hub.docker.com/r/edxops/analytics_pipeline_spark_master)
- [sparkworker](https://hub.docker.com/r/edxops/analytics_pipeline_spark_worker)
- [vertica](https://hub.docker.com/r/sumitchawla/vertica)
- [analyticspipeline](https://hub.docker.com/r/edxops/analytics_pipeline)

Repositorios que utiliza:

- [edx-analytics-pipeline](https://bitbucket.org/lcuelloalfa/edx-analytics-pipeline)

### Instalación Analytics Devstack ###

1. Para la instalación de **Docker Analytics Devstack**, ejecutar los pasos 1 a 4 de Instalación de **Docker edX Devstack**

2. Verificar que está en la rama correcta, exportar la variable `OPENEDX_RELEASE`:

	```
	export OPENEDX_RELEASE=hawthorn.master
	```

3. Configurar el **Analytics Devstack** usando el siguiente comando de provision:

	```
	make dev.provision.analytics_pipeline
	```

4. Para iniciar el servicio de **analytics**, usar el siguiente comando:

	```
	make dev.up.analytics_pipeline
	```

	Este comando monta los repositorios bajo el directorio `DEVSTACK_WORKSPACE`
	
5. Entrar a la máquina **analytics_pipeline** ejecutando el siguiente comando:

	```
	make analytics-pipeline-shell
	```

6. Para detener el servicio de **analytics**, usar el siguiente comando:

	```
	make stop.analytics_pipeline
	```

7. Al finalizar la instalación, entrar en analytics_pipeline desde devstack con `make analytics-pipeline-shell` e instalar las librerías Python necesarias para ejecutar los script de importación MySQL 

	```
	pip install mysql-python isodate
	```

8. Se debe editar o crear un usuario en MySQL con permisos para crear bases de datos y lectura de la base de datos edxapp, en este caso se editará el usuario existente `pipeline001`:

	```
	 GRANT ALL PRIVILEGES ON *.* to 'pipeline001'@'%';
	```
	
	**NOTA**: En el devstack esto se realiza desde la carpeta devstack y ejecutando el comando `make mysql-shell`, luego entrar a mysql con el comando `mysql` y finalmente utilizando el comando de GRANT.

9. (Opcional) Configurar Oauth2 para ejecutar tareas específicas. 

	9.1 Entrar a la máquina de LMS desde devstack ejecutando el comando `make lms-shell`, luego ejecutar el siguiente comando:

		./manage.py lms \
			--settings=devstack_docker \
			create_oauth2_client  \
			http://localhost:9999 \
			http://localhost:9999/complete/edx-oidc/  \
			confidential \
			--client_name "Analytics Pipeline" \
			--client_id oauth_id \
			--client_secret oauth_secret \
			--user staff \
			--trusted

	**NOTA**: Este comando crea una autorización Oauth2 para el usuario `staff`, la URL utilizada no es importante en este caso.

	9.2 Entrar a la máquina analytics_pipeline desde devstack ejecutando el comando `make analytics_pipeline-shell` y modificar el archivo ubicado en `/edx/app/analytics_pipeline/analytics_pipeline/config/luigi_docker.cfg`

	9.3 Modificar la URL de LMS (por defecto es el nombre de la máquina docker: `http://edx.devstack.lms:18000/`), ubicar la seccion `[edx-rest-api]` y modificar la variable `auth_url`, lo mismo con la sección `[course-list]` y `[course-blocks]` en la variable `api_root_url`.
	
	
Usuarios y Contraseñas
-----------------------

El script de provisionamiento crea un superusuario en Django por cada servicio.
	
- Correo: edx@example.com
- Usuario: edx
- Password: edx

El LMS tambien incluye cuentas de demostración, las contraseñas para cada una de estas cuentas es `edx`.

| Usuario  | Correo				  | Contraseña  |
|----------|----------------------|-------------|
| audit    | audit@example.com    | edx		    |
| honor    | honor@example.com    | edx		    |
| staff    | staff@example.com    | edx			|
| verified | verified@example.com | edx			|

Resolución de problemas
------------------------

### Problemas edX Devstack ###

- En el caso que LMS no inicie (no se visualiza el sitio en http://localhost:18000/) verificar lo que arrojan los logs en LMS

	```
	make lms-logs
	```

- Si arroja el siguiente error `Errno=No space left on device (ENOSPC)`, entrar a la maquina lms:

	```
	make lms-shell
	```

- Verificar el limite de user watches:

	```
	cat /proc/sys/fs/inotify/max_user_watches
	```

	**NOTA:** Por defecto este valor es de **8192**

- Aumentar el límite a **16384:**

	```
	sudo sysctl fs.inotify.max_user_watches=16384
	```

- Para que el cambio sea **permanente:**

	```
	echo 16384 | sudo tee -a /proc/sys/fs/inotify/max_user_watches
	```

- Reiniciar LMS:

	```
	make lms-restart
	```

### Problemas Analytics Devstack ###

* **Comando no encontrado: launch-task / remote-task** 

	Esto se debe principalmente a que, al crear la máquina Docker, los permisos de las carpetas no son actualizados, al ejecutar `ls -al` aparece como usuario sysadmin grupo exadmin, cuando deberia aparecer usuario hadoop grupo hadoop (este es el usuario por defecto de la máquina)

	- Probar si los comandos son reconocidos por la máquina:

		```
		launch-task ImportAuthUserTask --local-scheduler
		```

	- Entrar a la máquina analytics_pipeline desde el devstack utilizando `make analytics_pipeline-shell`, luego cambiar los permisos del directorio `/edx/app/analytics_pipeline/analytics_pipeline` (este es el directorio por defecto al entrar a la máquina) 

		```
		sudo chown -R hadoop:hadoop .
		```

	Volver a probar los comandos, si aun aparece el error realizar el siguiente paso

	- Ejecutar `make develop` para volver a instalar las librerias de Python

		```
		make develop
		```

		Volver a probar los comandos.


- Si ocurre algún problema durante la instalación, al utilizar `make dev.provision.analytics_pipeline`,  entrar a la máquina MySQL desde la carpeta devstack usando `make mysql-shell`, luego abrir `mysql` desde shell y eliminar la base de datos de **Hive**:

	```
	drop database edx_hive_metastore;
	```

	Luego volver a ejecutar `make dev.provision.analytics_pipeline`

